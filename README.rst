===================
Klok: Multi Scraper
===================
Klok is a simple command line tool built to use all scraper made available in klok.packages.

Documentation
-------------
The documentation is available at `this adresse`_.

.. _this adresse: https://klok.readthedocs.io/en/latest/