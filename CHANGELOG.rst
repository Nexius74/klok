===============================
Changelogs - 01.09.2020
===============================

0.1.0
=====
* Update cli to live in __main__.py
* Added time elapsed on fetch in cli
* Added sphinx doc generator to dev dependencies
