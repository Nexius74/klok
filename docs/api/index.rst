Developer Interface
-------------------
This part of the documentation covers all the interfaces of Klok. For parts where Klok depends on external libraries, we document the most important right here and provide links to the canonical documentation.

.. toctree::
    :maxdepth: 3

    generics