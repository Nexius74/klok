Generics
========
Define all core base class

BaseScraper
-----------
  Abstract class that define needed variables, methods for a simple webscraper

  Usage:

  .. code-block:: python

      from generics import BaseScraper

      class MyScraper(BaseScraper):
          ...

BaseScraper.\ **extract_text**\ (\ *css_selector*\ , *many*\ , *\*\*kwargs*\ )
  Extract html content of a tag based on given CSS selector

  | **Parameters**\ :
  |   **css_selector** - String representing a css selector
  |   **many** - (optional : default = False) If True, will grab content from multiple tag.

  | **Return**\ :
  |   **list** - list of content scrapped

  Usage:

  .. code-block:: python

      from generics import BaseScraper

      class MyScraper(BaseScraper):
          ...

          def fetch_data(self, url: str) -> None:
              """Fetch html data
              """
              some_variable = self.extract_text('.navbar > .navbar-item')
