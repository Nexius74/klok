CLI
===
This part of the documentation cover the command line interface of Klok. If you don't need to build a scraper but only use one available then this tool is made for you.

First make sure that:

* Klok is :doc:`installed <../install>` and up to date.

**Basic Usage**

.. code-block:: sh

    $ python klok 