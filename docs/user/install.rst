====================
Installation of Klok
====================
This part of the documentation covers the installation of Klok. The first step to using any software package is getting it properly installed.

Get the Source code
-------------------
Klok is actively developed on GitHub, where the code is `always available`_.

.. _always available: https://gitlab.com/Nexius74/klok/

You can clone the public repository:

.. code-block:: sh

  $ git clone https://gitlab.com/Nexius74/klok.git
  $ cd klok

Once you have a copy of the source, you can install it to your site-packages easily

* with poetry

.. code-block:: sh

  $ poetry build
    