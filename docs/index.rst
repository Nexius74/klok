================================
Klok: Multi Scraper
================================
Klok is a simple command line tool built to use all scraper made available in klok.packages.

The User Guide
--------------
This part of the documentation, which is mostly prose, begins with some background information about Klok, then focuses on step-by-step instructions for getting the most out of Klok.

.. toctree::
   :maxdepth: 3

   user/install
   user/quickstart/index

The API Documentation / Guide
-----------------------------
If you are looking for information on a specific function, class, or method, this part of the documentation is for you.

.. toctree::
   :maxdepth: 3

   api/index
   
There are no more guides. You are now guideless. Good luck.
