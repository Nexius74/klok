import argparse
import time

from klok.packages import mngkakalot

def main():
    actions = ['fetch', 'download']
    sources = ['hiperdex', 'mangakakalot', 'tsumino']
    parser = argparse.ArgumentParser(description='A simple cli tool to use all the scrapers')
    parser.version = '0.0.1'
    parser.add_argument(
        '-a',
        '--action',
        choices=actions,
        required=True,
        help='<fetch>: Download metadata and display it. <download>: Download the manga'
    )
    parser.add_argument(
        '-s',
        '--source',
        required=True,
        choices=sources,
        type=str,
        help='Manga website name'
    )
    parser.add_argument('-o', '--output', required=True, type=str, help='Path to download location. Doesn\'t do anything for fetch action')
    parser.add_argument('-u', '--urls', type=str, nargs='*', required=True, help='Specify url of the manga')
    parser.add_argument('-v', '--version', action='store_true', help='(Optional) Display cli version')
    args = parser.parse_args()

    if args.source == 'mangakakalot':
        if args.action == 'fetch':
            for url in args.urls:
                start = time.time()
                m = mngkakalot.MngKakalotScraper().fetch(url).pprint()
                end = time.time()
                print('Done in %s seconds' % str(end - start)[0:4])
        elif args.action == 'download':
            for url in args.urls:
                m = mngkakalot.MngKakalotScraper().download(url, args.output)

if __name__ == '__main__':
    main()
