import logging
import os
import pickle
import re
from datetime import datetime
from uuid import uuid4

import requests
from bs4 import BeautifulSoup

from klok.mixins import MixinMangaProgress
from klok.exceptions import GenericExceptions
from klok.utils import sanitize_string


class Colors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class DefaultScraper(object):
    """Class that define utils / misc methods for any scraper.

    Attributes
    ----------
        logger : Logger
            Logger object that will be used to print log message
        s : request.Session
            A simple HTTP client that handles cookies automatically
        uuid : UUID
            Give a random ID each time a scraper is created
        url : str
            Url to website to scrape
    
    Methods
    -------
        extract_text(soup, select)
            Extract text content of the selected tag
        extract_number(soup, select, many = False)
            Extract text content of the selected tag and cast it to float
    """
    def __init__(self, debug: bool = False):
        """Define local variables at object creation

        Parameters
        ----------
            debug : bool
                If True show all debug message while running

        Returns
        -------
            None
        """
        # Setup logger
        logging.basicConfig(format='[%(levelname)s] | %(asctime)-15s | %(message)s')
        self.__debug = debug
        self.log = logging.getLogger(__name__)
        self.s = requests.Session()
        self.uuid = uuid4()
        self.url = None
        if self.__debug:
            self.log.setLevel(logging.DEBUG)
        else:
            self.log.setLevel(logging.INFO)

    """
    Public methods
    """
    # FIXME : Doesn't filter correctly br tag. 
    #         Actual : some text.brbrBegin other text
    #         Expected : some text. Begin other text
    @staticmethod
    def extract_text(
        soup: BeautifulSoup,
        selector: str,
        many: bool = False,
        filter_escape: bool = False,
        regex: str = '',
        regex_group: int = 1):
        """Extract text content from selected tag

        Parameters
        ----------
            soup : BeautifulSoup
                Parsed html response by BeautifulSoup object
            selector : str
                CSS selector
            many : bool
                If True allow to grab text from multiple tag
            filter_escape : bool
                Define if res should be filtered from escapable character like \\n
            regex : str
                Give a regex to filter result before return
            regex_group : int
                Defin which group of the regex pattern must be returned

        Raises
        ------
            AttributeError
                Raise this error if given regex pattern doesn't match any element

        Returns
        -------
            list
                Tag(s) content
        """
        if many:
            res = [sanitize_string(x.text) for x in soup.select(selector) if x is not None]
        else:
            soup_selected = soup.select_one(selector)
            res = [sanitize_string(soup_selected.text)] if soup_selected is not None else None

        if res:
            if regex != '':
                res = [re.search(regex, repr(x)).group(regex_group) for x in res]
            
            # FIXME : Even if .replace('\\n') is in .sanitize_string() it doesn't work
            if filter_escape:
                if isinstance(res, list):
                    res = [x.replace('\\n', '').replace('\\t', '') for x in res]
                else:
                    res = res.replace('\\n', '').replace('\\t', '')
        else:
            raise GenericExceptions.ElementNotFoundError('CSS selector "{}" return no element'.format(selector))
        return res

    @staticmethod
    def extract_number(soup: BeautifulSoup, selector: str, many: bool = False, regex: str = '', regex_group : int = 1):
        """Extract text content from selected tag and cast it to float

        Parameters
        ----------
            soup : BeautifulSoup
                Parsed html response by BeautifulSoup object
            selector : str
                CSS selector
            many : bool
                If True allow to grab text from multiple tag
            regex : str
                Find sub string in res by given a valid regex pattern
            regex_group : int
                Defin which group of the regex pattern must be returned

        Raises
        ------
            AttributeError
                Raise this error if given regex pattern doesn't match any element

        Returns
        -------
            any
                Tag content that should be represented as a float object
        """
        if regex != '':
            try:
                res = [float(re.search(regex, repr(x.text)).group(regex_group)) for x in soup.select(selector)] if many else float(re.search(regex, repr(soup.select_one(selector).text)).group(regex_group))
            except AttributeError:
                raise AttributeError('Regex pattern return None')
        else:
            res = [float(x.text) for x in soup.select(selector)] if many else float(soup.select_one(selector).text)
        return res 

    @staticmethod
    def extract_attr(soup: BeautifulSoup, selector: str, attr: str, filter_escape: bool = False, many: bool = False):
        """Extract Tag attribute value

        Parameters
        ----------
            soup : BeautifulSoup
                Parsed html response by BeautifulSoup object
            selector : str
                CSS selector
            attr : str
                The name of the attribute to scrap
            filter_escape : bool
                Define if res should be filtered from escapable character like \\n
            many : bool
                If True allow to grab text from multiple tag

        Returns
        -------
            any
                Tag attribute
        """
        res = [x.get(attr) for x in soup.select(selector)] if many else soup.select_one(selector).get(attr)
        
        if filter_escape:
            res = [x.strip() for x in res] if isinstance(res, list) else res.strip()

        return res


class MangaScraper(DefaultScraper, MixinMangaProgress):
    """Class that act as a boiler plate to create manga scraper.

    Attributes
    ----------
        name : str
            The name of the scraped manga
        author : str
            The author name of the scraped manga
        description : str
            The description of the scraped manga
        rating : str
            The rating of the scraped manga (Maximum value is 10)
        upload_date : str
            The upload date of the scraped manga
        tags : list
            The tags associated to the scraped manga

    Methods
    -------
        fetch(url)
            Method where scraping of all the metadata will happened
        download(url, dest = OUTPUT_DIR)
            Method where the download of the manga will happened
    """
    OUTPUT_DIR = os.getcwd()

    def __init__(self):
        """Define local variables at object creation

        Returns
        -------
            None
        """
        DefaultScraper.__init__(self)
        MixinMangaProgress.__init__(self)
        self.name = []
        self.author = []
        self.description = []
        self.rating = 0.0
        self.upload_date = 'unknown'
        self.tags = 'unknown'

    def pprint(self):
        """Display metadata in a better way
        """
        window = """
Provided by Klok - 0.0.1a

-----------------------< {0}Manga Metadata{1} >-----------------------

{0}Name{1}: {3}\n
{0}Author(s){1}: {4}\n
{0}Description{1}: {5}\n
{0}Rating{1}: {6} / 10\n
{0}Tags{1}: {7}\n
{0}Upload date{1}: {8}
-----------------------------< {0}End{1} >----------------------------
        """.format(
            Colors.OKGREEN,
            Colors.ENDC,
            Colors.OKBLUE,
            self.name,
            self.author,
            self.description,
            self.rating,
            self.tags,
            self.upload_date
            )
        print(window)

    """
    Methods to override
    """
    def fetch(self, url: str):
        """Class method where the scraping of metadata will happened

        This method must be override and should return self

        Parameters
        ----------
            url : str
                Link to the wanted manga. E.g. https://www.tsumino.com/entry/48972/

        Raises
        ------
            NotImplementedError
                This method must be override
        """
        raise NotImplementedError('This method must be override.')

    def download(self, url: str, dest: str = OUTPUT_DIR):
        """Class method where the download of the manga will happened

        This method must be override

        Parameters
        ----------
            url : str
                Link to the wanted manga. E.g. https://www.tsumino.com/entry/48972/
            dest : str, optional
                Directory where the manga will be downloaded, by default OUTPUT_DIR

        Raises
        ------
            NotImplementedError
                This method must be override
        """        
        raise NotImplementedError('This method must be override.')

    """
    Protected methods
    """
    def _save(self):
        """Save current metadata into a pickle file

        Returns
        -------
            None
        """        
        if self.name:
            path = os.path.join(os.path.dirname(os.path.dirname(OUTPUT_DIR)), 'backups')
            if not os.path.exists(path):
                os.makedirs(path)
            with open(os.path.join('backups', 'obj_{}_{}.dat'.format(self.name.lower(), datetime.now().strftime('%m%d%Y'))), 'wb') as h:
                pickle.dump(self, h)

    @staticmethod
    def _load(path: str):
        """Loads a pickle into a scraper object

        Parameters
        ----------
            path : str
                Path to a pickle file

        Returns
        -------
            None
        """
        if os.path.exists(path):
            with open(path) as h:
                return pickle.load(h)
