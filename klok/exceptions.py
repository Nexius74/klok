class GenericExceptions:
    """Base class for all base scraper exception
    """

    @staticmethod
    class ElementNotFoundError(BaseException):
        """Raised when an html element couldn't be found using the CSS selector
        """
        pass