import time

class MixinProgress(object):
    """Give the ability to track down progresse during a download process
    """
    def __init__(self):
        self.start_time = 0
        self.end_time = 0
        self.elapsed_time = 0
        self.total = 0
        self.current = 0

    def __str__(self):
        return '%s / %s' % (self.current, self.total)

    def get_percent(self):
        """Return current in percent

        Returns
        -------
            int
                A value in percent of current
        """
        return self.current * 100 / self.total

    def get_elapsed_time(self):
        """[summary]
        """
        return (self.end_time - self.start_time)

    def set_timer(self):
        """[summary]
        """
        self.start_time = time.time()[0:4]    

    def stop_timer(self):
        """[summary]
        """
        self.end_time = time.time()[0:4]


class MixinMangaProgress(MixinProgress):
    """Give the ability to track down how many chapter or pages have been process in a download

    Parameters
    ----------
        Progress : Progress
            Parent class
    """
    def __init__(self):
        self.progress_chapter = MixinProgress()
        self.progress_page = MixinProgress()
