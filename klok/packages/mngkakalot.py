import os
import re
from shutil import rmtree

from klok import generics, validators

from bs4 import BeautifulSoup


class MngKakalotScraper(generics.MangaScraper):
    """Class that allows to fetch or download data from https://www.mangakakalots.com/
    """
    def fetch(self, url: str):
        """Scrape metadata

        Parameters
        ----------
            url : str
                Link to the wanted manga. E.g. https://mangakakalot.com/read-er1un158524520741

        Returns
        -------
            self
                Allows method chaining / cascading
        """
        # Check url
        if validators.url(url) and 'https://mangakakalot.com/' in url:
            # Request onto url
            res = self.s.get(url)
            if res.status_code != 200:
                raise ConnectionError('Unable to contact {} with status code {}'.format(url, res.status_code))

            # Parse response content
            soup = BeautifulSoup(res.content, 'html.parser')

            # Extract data
            self.url = url
            self.name = self.extract_text(soup, '.manga-info-text > li > h1')
            self.author = self.extract_text(soup, '.manga-info-text > li > a[href^="https://mangakakalot.com/search/author"]', many=True)
            self.description = self.extract_text(soup, '#noidungm', filter_escape=True, regex=r'(?<=\\n)(.*)')
            self.upload_date = re.search(r'(?<=updated..)(\w+.\d+.\d+)', self.extract_text(soup, '.manga-info-text > li', many=True)[3]).group(1)
            self.rating = self.extract_number(soup, '#rate_row_cmd', regex=r'(?<=:)(.*)(?=\/)') * 2
            self.tags = self.extract_text(soup, '.manga-info-text > li > a[href^="https://mangakakalot.com/manga_list"]', many=True)

        return self

    def download(self, url: str, dest: str = generics.MangaScraper.OUTPUT_DIR):
        """Download manga

        Parameters
        ----------
            url : str
                Url to the manga to download
            dest : str, optional
                Directory where manga will be downloaded, by default OUTPUT_DIR
        """
        self.fetch(url)
        output_dir = os.path.join(dest, self.name[0] if self.name else 'default_%s' % self.uuid)

        if not os.path.exists(dest):
            os.makedirs(dest)

        # Request onto url
        res = self.s.get(url)
        if res.status_code != 200:
            raise ConnectionError('Unable to contact {} with status {}'.format(url, res.status_code))

        # Parse response content
        soup = BeautifulSoup(res.content, 'html.parser')

        # Get all chapters url
        self.__chapters_url = self.extract_attr(soup, '.chapter-list > .row > span > a', 'href', many=True)
        # Reverse order of url
        self.__chapters_url.reverse()
        # Define total progress to keep track on download process
        self.progress_chapter.total = len(self.__chapters_url)
        # Build payload for later use
        self.chapters = zip(
            ['Chapter {}'.format(idx) for idx, _ in enumerate(self.__chapters_url, start=1)],
            self.__chapters_url,
        )

        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        # Process all chapters found
        for idx, (ch_name, ch_url) in enumerate(self.chapters, start=1):
            # Define current progress to keep track on download process
            self.progress_chapter.current = idx

            res = self.s.get(ch_url)
            if res.status_code != 200:
                raise ConnectionError('Unable to contact {} with status {}'.format(url, res.status_code))

            soup = BeautifulSoup(res.content, 'html.parser')
            img_urls = self.extract_attr(soup, '#vungdoc img', 'src', many=True)

            # Define total page progress to keep track on download process
            self.progress_page.total = len(img_urls)

            ch_path = os.path.join(output_dir, ch_name)

            # Check if chapter directory has already been downloaded
            if not os.path.exists(ch_path):
                os.makedirs(ch_path)
                # Process all image found in chapter
                for idx, img_url in enumerate(img_urls, start=1):
                    self.download_image(idx, url, img_url, output_dir, ch_name)
            elif len(os.listdir(ch_path)) != len(img_urls):
                # Check if length content in chapter directory equal length chapter url scraped
                rmtree(ch_path)
                os.makedirs(ch_path)
                # Process all image found in chapter
                for idx, img_url in enumerate(img_urls, start=1):
                    self.download_image(idx, url, img_url, output_dir, ch_name)
            else:
                self.log.info('Skipping Chapter %s' % idx)


    def download_image(self, idx: int, ref_url: str, img_url: str, output_dir: str, ch_name: str):
        """[summary]
        """
        # Define total page progress to keep track on download process
        self.progress_page.current = idx

        self.log.info('Chapter: {} / {} | Page: {} / {}'.format(
            self.progress_chapter.current,
            self.progress_chapter.total,
            self.progress_page.current,
            self.progress_page.total
        ))

        res = self.s.get(img_url, headers={'referer': ref_url})
        if res.status_code != 200:
            raise ConnectionError('Unable to contact {} with status {}'.format(img_url, res.status_code))

        with open(os.path.join(output_dir, ch_name, '{}.jpg'.format(idx)), 'wb') as h:
            h.write(res.content)