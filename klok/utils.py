def sanitize_string(string: str) -> str:
    """Remove all file system forbidden caracter from a string

    Parameters
    ----------
        string : str
            Any string

    Returns
    -------
        str
            Sanitized string
    """    
    return string.replace(':', '').replace('*', 'x').replace('?', '')\
            .replace('<', '').replace('>', '').replace('\t', '').replace('\\', '')\
            .replace('/', '').replace('|', '').replace('[', '(').replace(']', ')')\
            .replace('"', '').replace('\\n', '').replace(',', '').replace('\'', '')\
            .strip()
