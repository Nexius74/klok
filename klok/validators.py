from urllib.parse import urlparse

def url(url: str) -> bool:
    """Url validation

    Parameters
    ----------
        url : str
            HTTP link

    Returns
    -------
        bool
            True if url is valid
    """    
    o = urlparse(url)
    if o.scheme != 'http' and o.scheme != 'https':
        return False
    if o.netloc == '':
        return False
    return True
